CXX = g++
CC = gcc
OBJ = mobile.o loadshader.o loadtexture.o random.o objparser.o list.o stringextra.o
CFLAGS = -ggdb -Wall -I./glm
CXXFLAGS = ${CFLAGS}

LDLIBS=-lm -lglut -lGLEW -lGL -pthread

mobile: $(OBJ)
	 $(CXX) -o $@ $^ $(CXXFLAGS) $(LDLIBS)
clean:
	rm -f *.o mobile 
.PHONY: all clean

