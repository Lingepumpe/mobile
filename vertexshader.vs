#version 330

uniform mat4 modelViewMatrix; //view * model
uniform mat4 modelViewProjectionMatrix; //projection * view * model
uniform mat3 normalMatrix;
uniform vec3 lightNormalPosition;
uniform vec3 lightColouredPosition;
uniform vec3 colouredIntensity;
uniform vec3 calculationVector;
uniform int  shadingType;
uniform vec4 reflectionProperties;
uniform int  hasTexture;

layout (location = 0) in mediump vec3 position;
layout (location = 1) in mediump vec3 normal;
layout (location = 2) in mediump vec2 texture;
layout (location = 3) in mediump vec3 tangent;
layout (location = 4) in mediump vec3 bitangent;
layout (location = 5) in vec3 color;

out mediump vec3 ourCoords3;
out mediump vec2 uvCoords;
out vec3 vColor;

out mediump vec3 inputtnorm;
out mediump vec3 ttang;
out mediump vec3 tbita;

void main()
{
   uvCoords = texture;
   inputtnorm = normalize(normalMatrix * normal);
   ttang = normalize(normalMatrix * tangent);
   tbita = normalize(normalMatrix * bitangent);
   vec4 ourCoords4 = modelViewMatrix * vec4(position, 1.0);
   ourCoords3 = vec3(ourCoords4.x, ourCoords4.y, ourCoords4.z);
   vec3 l1 = normalize(lightColouredPosition - ourCoords3);
   vec3 l2 = normalize(lightNormalPosition - ourCoords3);
   vec3 v = normalize(-ourCoords3);
   vec3 r1 = reflect(-l1, inputtnorm);
   vec3 r2 = reflect(-l2, inputtnorm);
   float sDotN1 = max(dot(l1, inputtnorm), 0.0);
   float sDotN2 = max(dot(l2, inputtnorm), 0.0);

   vec3 intensity = vec3(0, 0, 0);
   if(calculationVector.x > 0.5) { //ambient lighting enabled
      intensity += reflectionProperties.x * colouredIntensity;
   }
   if(calculationVector.y > 0.5) { //diffuse lighting enabled
      intensity += reflectionProperties.y * colouredIntensity * sDotN1;
      intensity += reflectionProperties.y * vec3(0.3, 0.3, 0.3) * colouredIntensity * sDotN2;
   }
   if(calculationVector.z > 0.5) { //specular lighting enabled
      if(sDotN1 > 0.0) {
         intensity += reflectionProperties.z * colouredIntensity * pow(max(dot(r1, v), 0.0), reflectionProperties.w);
      }
      if(sDotN2 > 0.0) {
         intensity += reflectionProperties.z * vec3(0.3, 0.3, 0.3) * pow(max(dot(r1, v), 0.0), reflectionProperties.w);
      }
   }

   gl_Position = modelViewProjectionMatrix*vec4(position.x, position.y, position.z, 1.0);
   if(shadingType == 1) { //phong shading requested
     vColor = color;
   } else {
     if(hasTexture == 1) {
       vColor = intensity;
     } else {
       vColor = color*intensity;
     }
   }
}
