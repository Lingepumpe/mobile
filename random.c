//system
#include <stdlib.h>
#include <time.h>
#include <assert.h>


//own

#include "random.h"

void initRandom() {
    srand(time(NULL));
}

int getRandomNumber(int from, int to) {
    assert(to - from <= RAND_MAX);
    int myRand = rand();
    if(myRand + 1 + to - from > RAND_MAX) { //one of the dangerous numbers close to the "edge" of random
        return getRandomNumber(from, to); //try again
    }
    return myRand % (1 + to - from) + from;
}
